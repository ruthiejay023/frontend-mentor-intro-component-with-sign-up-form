import CardComponent from '../CardComponent.vue'

describe('<CardComponent />', () => {
  it('renders', () => {
    // see: https://on.cypress.io/mounting-vue
    cy.mount(CardComponent)
    cy.get("form input")
      .should("have.length", 4)
    cy.get("button").should("contain", "Claim your free trial")
    
    // cy.get("form input").should("contain", "w-full.outline-none")
  })
})